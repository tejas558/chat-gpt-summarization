import { useState } from 'react';
import './App.css';
import { Configuration, OpenAIApi } from "openai";

function App() {
  const [text, setText] = useState("");
  const [result, setResult] = useState("");
  const [key, setKey] = useState("");
  const sendRequest = async () => {
    const configuration = new Configuration({
      apiKey: key,
    });
    const openai = new OpenAIApi(configuration);

    const completion = await openai.createCompletion({
      model: "text-davinci-003",
      prompt: `${text} Tl;dr ${result}`,
      temperature: 0.7,
      max_tokens: 100,
    });
    var str = completion.data.choices[0].text;
    setResult(result + str);

    console.log(text);
    console.log(completion.data);
  }
  return (
    <div className="App">
      <h1>Paragraph Summarizer</h1>
      <textarea onChange={e => setKey(e.target.value)} placeholder={`Enter OpenAi Api Key here`}/>
      <br></br>
      <textarea style={{width : "400px", height : "100px", marginTop : "10px"}} onChange={e => setText(e.target.value)}/>
      <br></br>
      <button style={{marginTop : "5px"}} onClick={() => sendRequest()}>Submit</button>
      <p style={{marginTop : "10px"}}>{result}</p>
    </div>
  );
}

export default App;
